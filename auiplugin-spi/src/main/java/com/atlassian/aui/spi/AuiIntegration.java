package com.atlassian.aui.spi;

import java.io.Serializable;

/**
 * Enables web applications to integrate with the AUI plugin.
 *
 * @since 4.0
 */
public interface AuiIntegration {

    /**
     * <p>Returns the context path of the web application.</p>
     * <p>The path starts with a "/" character but does not end with a "/" character. For servlets in the default (root) context, this method returns "".</p>
     *
     * @return The context path of the web application, or "" for the default (root) context
     */
    public String getContextPath();

    /**
     * <p>Retrieves the unformatted internationalised message associated with the supplied key.</p>
     * <p>If no such association exists, this method returns the key.</p>
     *
     * @param key The internationalised message key
     *
     * @return The unformatted internationalised text associated with the key, or the key if no such association exists
     */
    public String getRawText(String key);

    /**
     * <p>Retrieves the formatted internationalised message associated with the supplied key.</p>
     * <p>If no such association exists, this method returns the formatted key.</p>
     *
     * @param key The internationalised message key
     * @param arguments An optional list of formatting arguments
     *
     * @return The formatted internationalised text associated with the key, or the formatted key if no such association exists
     */
    public String getText(String key, Serializable... arguments);

}
