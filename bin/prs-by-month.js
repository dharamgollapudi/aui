/**
 * Produces a table of completed pull requests by month.
 * PRs are discovered by finding all commits matching a regex.
 *
 * usage: node ./bin/prs-by-month.js
 */

var exec = require('child_process').exec;

var commitsByMonth = {
    _map: {},
    add: function(date) {
        var month = date.getMonth() + 1;
        var key = date.getFullYear() + '-' + (month < 10 ? '0' : '') + month;
        var map = this._map;
        if (!map.hasOwnProperty(key)) {
            map[key] = 0;
        }
        ++map[key];
    },
    write: function(out) {
        var map = this._map;
        Object.keys(map).sort().reverse().forEach(function(key) {
            out.write(key + '\t' + map[key] + '\n');
        });
    }
};

exec('git log -E -i --grep="Merged in.*pull request" --format="%ct %s"',
    function (error, stdout, stderr) {
        if (error !== null) {
            console.log('error: ' + error);
        }
        var lines = stdout.split('\n');
        lines.forEach(function(line) {
            var dateStr = line.slice(0, line.indexOf(' '));
            if (!dateStr) {
                return;
            }
            var date = new Date(dateStr * 1000);
            commitsByMonth.add(date);
        });
        commitsByMonth.write(process.stdout);
    }
);