Qunit.require('js/external/jquery/jquery.js');
Qunit.require('js/atlassian/atlassian.js');
Qunit.require('js/atlassian/internal/widget.js');
Qunit.require('js/atlassian/dialog2.js');

Qunit.require('dependencies/js/soyutils.js');
Qunit.require('soy/atlassian.soy.js');
Qunit.require('soy/dialog2.soy.js');

module("Dialog2 soy tests");

test("Dialog2 does creates close button for non-modal", function() {
    var $el = AJS.$(aui.dialog.dialog2({
        content: "hello world"
    }));

    ok($el.find(".aui-dialog2-header-close").length, "Expected a close button to be rendered");
});

test("Dialog2 does not create close button for modal", function() {
    var $el = AJS.$(aui.dialog.dialog2({
        content: "hello world",
        modal: true
    }));

    ok(!$el.find(".aui-dialog2-header-close").length, "Expected no close button to be rendered");
});


module("Dialog2 tests", {
    setup: function() {
        AJS._internal = {
            browser: {
                supportsCalc: function() { return true; }
            }
        };
    },
    // Creates a mock of a layer object. AJS.layer will return this when passed the given $el
    createLayerMock: function($el) {
        var layerInstance = {
            show: function() {},
            hide: function() {},
            remove: function() {},
            on: function() {}
        };
        // Dirty stubbing for AJS.layer.
        // Can't use sinon.stub.withArgs() as it only takes exact matches and we need
        // to match on the underlying dom element (ie $el[0] === $popupEl[0])
        var oldLayer = AJS.layer;
        AJS.layer = function($layerEl) {
            if ($el[0] === $layerEl[0]) {
                return layerInstance;
            }
            else {
                // oldLayer will exist if createLayerMock is called multiple times in the same test
                return oldLayer && oldLayer($popupEl);
            }
        }
        return sinon.mock(layerInstance);
    },
    createContentEl: function() {
        return AJS.$(aui.dialog.dialog2({
            content: "Hello world"
        })).appendTo("#qunit-fixture");
    },
    teardown: function() {
        AJS.layer = null;
    }
});

test("Dialog2 creates a dialog with given content", function() {
    var $el = this.createContentEl();

    var dialog = AJS.dialog2($el);

    equal($el[0], dialog.$el[0], "Dialog2 accepts given $el");
});

test("Dialog2 wraps layer for show, hide, remove", function() {
    var $el = this.createContentEl();
    var dialog = AJS.dialog2($el);
    var layerMock = this.createLayerMock(dialog.$el);
    layerMock.expects("show").once();
    layerMock.expects("hide").once();
    layerMock.expects("remove").once();

    dialog.show();
    dialog.hide();
    dialog.remove();

    layerMock.verify();
});

test("Dialog2 hide is called on close button click", function() {
    var $el = this.createContentEl();
    var $close = AJS.$("<div></div>").addClass("aui-dialog2-header-close").appendTo($el);
    var dialog = AJS.dialog2($el);
    var layerMock = this.createLayerMock(dialog.$el);
    layerMock.expects("hide").once();
    dialog.show();

    $close.click();

    layerMock.verify();
});

test("Dialog2 wraps layer events", function() {
    var $el = this.createContentEl();
    var dialog = AJS.dialog2($el);
    var layerMock = this.createLayerMock(dialog.$el);
    var fn = function() {};
    layerMock.expects("on").once().withArgs("show", fn);

    dialog.on("show", fn);

    layerMock.verify();
});

