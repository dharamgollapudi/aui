Qunit.require('js/external/jquery/jquery.js');
Qunit.require('js/external/jquery/serializetoobject.js');
Qunit.require('js/external/underscorejs/underscore.js');
Qunit.require('js/external/backbone/backbone.js');

Qunit.require('js/atlassian/atlassian.js');
Qunit.require('js/atlassian/format.js');

Qunit.require('dependencies/js/soyutils.js');
Qunit.require('soy/atlassian.soy.js');
Qunit.require('soy/icons.soy.js');

Qunit.require('js/atlassian/experimental-events/events.js');

Qunit.require('js/atlassian/experimental-restfultable/restfultable.js');
Qunit.require('js/atlassian/experimental-restfultable/restfultable.entrymodel.js');
Qunit.require('js/atlassian/experimental-restfultable/restfultable.row.js');
Qunit.require('js/atlassian/experimental-restfultable/restfultable.editrow.js');
Qunit.require('js/atlassian/experimental-restfultable/restfultable.customview.js');


module('Initialization', {
    setup: function() {

        this.server = sinon.fakeServer.create();
        var users = [{"id":1, "name": "adam"},{"id":2, "name": "betty"},{"id":3, "name": "chris"}];

        this.server.respondWith("GET", "/all", [ 200, { "Content-Type": "application/json" }, JSON.stringify(users) ]);

        this.rt = new AJS.RestfulTable({
            el: $('<table id="test-table" class="aui"></table>'),
            resources: {
                all: "/all",
                self: "/single"
            },
            columns: [
                {
                    id: "name",
                    header: "Name"
                }
            ]
        });

        this.rtRows = [];
        this.rt.bind(AJS.RestfulTable.Events.ROW_INITIALIZED, _.bind(function (row) {
            this.rtRows.push(row);
        }, this));

        this.$table = this.rt.getTable();
        this.$table.appendTo("#qunit-fixture");
        this.server.respond();
    }, teardown: function() {
        this.server.restore();
    }
});

test('renders properly', function () {
    equal(this.$table.length, 1, "Table element is rendered");
    equal(this.rtRows.length, 3, "All rows returned by the server are present");

    equal(this.$table.find("thead th:first").text(), "Name", "Proper column header is used");
    equal(this.$table.find("tbody.aui-restfultable-create tr").length, 1, "Create row is rendered");
    equal(this.$table.find("tbody:not(.aui-restfultable-create) tr").length, 3, "All rows returned by the server are rendered");
});

test('proper row data is used', function () {
    equal(this.rtRows[0].model.get("id"), 1, "Proper id is used");
    equal(this.rtRows[0].model.get("name"), "adam", "Proper column content is used");

    equal(this.$table.find("tbody:not(.aui-restfultable-create) tr:eq(0)").data("id"), 1, "Proper id is used");
    equal(this.$table.find("tbody:not(.aui-restfultable-create) tr:eq(0)").data("name"), "adam", "Proper column content is displayed");
    equal(this.$table.find("tbody:not(.aui-restfultable-create) tr:eq(1) td:first").text(), "betty", "Proper column content is displayed");
});

test('edit works', function () {
    var row = this.rtRows[0];
    var edited = this.rt.edit(row, "name");
    edited.$("input[name=name]").val("edited");
    edited.submit(false);

    this.server.respondWith("PUT", "/single/1", function (xhr, id) {
        var obj = JSON.parse(xhr.requestBody);
        xhr.respond(200, { "Content-Type": "application/json" }, xhr.requestBody);
    });
    this.server.respond();

    var editResponse = JSON.parse(this.server.requests[1].requestBody);
    equal(editResponse.name, "edited", "right data is being sent to the server");
});


test('fieldFocusSelector is defined for create row', function () {
    var row = this.rtRows[0];
    var edited = this.rt.edit(row, "name");
    edited.$("input[name=name]").focus();
    this.rt.getCreateRow().focus("name");

    ok(true, "fieldFocusSelector is defined (no exception was thrown by the previous statement)");
});

module('Custom object serialization', {
    setup: function() {

        this.server = sinon.fakeServer.create();
        var users = [{"id":1, "name": "adam"}];

        this.server.respondWith("GET", "/all", [ 200, { "Content-Type": "application/json" }, JSON.stringify(users) ]);
        this.rt = new AJS.RestfulTable({
            el: $('<table id="test-table" class="aui"></table>'),
            resources: {
                all: "/all",
                self: "/single"
            },
            columns: [
                {
                    id: "name",
                    header: "Name"
                }
            ],
            views: {
                editRow: AJS.RestfulTable.EditRow.extend({
                    initialize: function() {
                        AJS.RestfulTable.EditRow.prototype.initialize.apply(this, Array.prototype.slice.call(arguments));
                    },
                    serializeObject: function() {
                        return {
                            name: this.$el.find(":input[name=name]").val() + " serialized"
                        };
                    }
                })
            }
        });

        this.rtRows = [];
        this.rt.bind(AJS.RestfulTable.Events.ROW_INITIALIZED, _.bind(function (row) {
            this.rtRows.push(row);
        }, this));

        this.$table = this.rt.getTable();
        this.$table.appendTo("#qunit-fixture");
        this.server.respond();
    }, teardown: function() {
        this.server.restore();
    }
});

test('custom serializing works', function () {
    var row = this.rtRows[0];
    var edited = this.rt.edit(row, "name");
    edited.$("input[name=name]").val("edited");
    edited.submit(false);
    this.server.respondWith("PUT", "/single/1", function (xhr, id) {
        var obj = JSON.parse(xhr.requestBody);
        xhr.respond(200, { "Content-Type": "application/json" }, xhr.requestBody);
    });
    this.server.respond();

    var editResponse = JSON.parse(this.server.requests[1].requestBody);
    equal(editResponse.name, "edited serialized", "custom serialization method works properly");
});
