Qunit.require('js/external/jquery/jquery.js');
Qunit.require('js/atlassian/atlassian.js');
Qunit.require('js/atlassian/inline-dialog.js');

module("Inline Dialog Options");

test("Inline Dialog Creation", function() {
    var testInlineDialog = AJS.InlineDialog(AJS.$("body"), 1, function(){});
    ok(typeof testInlineDialog == "object", "Inline Dialog Successfully Created!");
    ok(typeof testInlineDialog.show == "function", "testInlineDialog has the show function.");
    ok(typeof testInlineDialog.hide == "function", "testInlineDialog has the hide function.");
    ok(typeof testInlineDialog.refresh == "function", "testInlineDialog has the refresh function.");
});

test("Inline Dialog getOptions function", function() {
    var testInlineDialog = AJS.InlineDialog(AJS.$("body"), 1, function(){}, {});
    ok(typeof testInlineDialog == "object", "Inline Dialog Successfully Created!");
    ok(typeof testInlineDialog.show == "function", "testInlineDialog has the show function.");
    ok(typeof testInlineDialog.hide == "function", "testInlineDialog has the hide function.");
    ok(typeof testInlineDialog.refresh == "function", "testInlineDialog has the refresh function.");
    ok(typeof testInlineDialog.getOptions == "function", "testInlineDialog has the getOptions function");
    ok(typeof testInlineDialog.getOptions() == "object", "getOptions function successfully returned an object");
});
test("Inline Dialog default options", function() {
    var testInlineDialog = AJS.InlineDialog(AJS.$("body"), 1, function(){});
    var expectedDefaults = {
        onTop: false,
        responseHandler: function(data, status, xhr) {
            //assume data is html
            return data;
        },
        closeOthers: true,
        isRelativeToMouse: false,
        addActiveClass: true,
        onHover: false,
        useLiveEvents: false,
        noBind: false,
        fadeTime: 100,
        persistent: false,
        hideDelay: 10000,
        showDelay: 0,
        width: 300,
        offsetX: 0,
        offsetY: 10,
        arrowOffsetX: 0,
        container: "body",
        cacheContent : true,
        displayShadow: true,
        preHideCallback: function () { return true; },
        hideCallback: function(){}, // if defined, this method will be exected after the popup has been faded out.
        initCallback: function(){}, // A function called after the popup contents are loaded. `this` will be the popup jQuery object, and the first argument is the popup identifier.
        upfrontCallback: function() {}, // A function called before the popup contents are loaded. `this` will be the popup jQuery object, and the first argument is the popup identifier.
        calculatePositions: function() {},
        getArrowPath: function() {},
        getArrowAttributes: function() {}
    };
    ok(typeof testInlineDialog == "object", "Inline Dialog Successfully Created!");
    var isDefault = true;
    // console.log(isDefault);
    AJS.$.each(testInlineDialog.getOptions(), function(index, value){
        if(expectedDefaults[index] != value && typeof value != "function" || typeof value != typeof expectedDefaults[index]){
            isDefault = false;
        }
    });
    // console.log(isDefault);
    ok(isDefault, "all default options are as expected");
    
});


module("Inline Dialog Positioning", {
    setup: function() {
        var popup = this.popup = AJS.$("<div id='dummy-popup'><span class='arrow'/></div>")
            .css({"width": "200px", "height": "100px"});

        var trigger = this.trigger = AJS.$("<span id='dummy-trigger'/>")
            .appendTo(AJS.$("#qunit-fixture"))
            .css({"width": "200px", "height": "20px", "display": "inline-block", "position": "fixed", "left": "10px", "top": "10px"});

        var mouse = this.mouse = { x: 0, y: 0 };
        var opts = this.opts = { offsetX: 0, offsetY: 0, arrowOffsetX: 0 }; // Will get NaN unless these are supplied. wtf.

        this.invoke = function() {
            return AJS.InlineDialog.opts.calculatePositions(popup, { target: trigger }, mouse, opts);
        };
    }
});

test("Returns object with positioning information", function() {
    var result = this.invoke();
    equal(typeof result, "object");

    equal(typeof result.popupCss, "object");
    ok(result.popupCss.hasOwnProperty("left"));
    ok(result.popupCss.hasOwnProperty("right"));
    ok(result.popupCss.hasOwnProperty("top"));

    equal(typeof result.arrowCss, "object");
    ok(result.arrowCss.hasOwnProperty("left"));
    ok(result.arrowCss.hasOwnProperty("right"));
    ok(result.arrowCss.hasOwnProperty("top"));
});

test("Popup left-aligned with trigger's left edge when trigger is smaller", function() {
    this.trigger.width(100);
    this.trigger.css("left", 10);
    this.popup.width(200);

    var result = this.invoke();
    equal(result.popupCss.left, 10);
});

test("Popup centre-aligned to trigger's width when trigger is larger", function() {
    this.trigger.width(400);
    this.trigger.css("left", 10);
    this.opts.width = 200; // fixme: wtf, srsly
    this.popup.width(this.opts.width);

    var result = this.invoke();
    equal(result.popupCss.left, 110, "should start at half width of trigger minus half width of popup plus trigger's offset");
});

test("Popup cannot be positioned further than 10px from window's right edge when popup would cause horizontal scrollbars", function() {
    this.trigger.width(100);
    this.trigger.css({"left": "auto", "right": 10});
    this.popup.width(200);

    var result = this.invoke();
    equal(result.popupCss.left, "auto");
    equal(result.popupCss.right, 10);
});

test("Popup arrow points to middle of trigger when popup is left-aligned", function() {
    this.trigger.width(100);
    this.trigger.css("left", 10);
    this.popup.width(200);

    var result = this.invoke();
    equal(result.arrowCss.left, 50);
});

test("Popup arrow points to middle of trigger when popup is centre-aligned", function() {
    this.trigger.width(100);
    this.trigger.css("left", 10);
    this.popup.width(200);

    var result = this.invoke();
    equal(result.arrowCss.left, 50);
});

test("Popup arrow points to middle of trigger when popup is right-aligned", function() {
    this.trigger.width(100);
    this.trigger.css({"left": "auto", "right": 10});
    this.popup.width(200);

    var result = this.invoke();
    equal(result.arrowCss.left, 150, "you'd think it'd be on the right, but we don't do that, so it's popup width minus the offset.");
    equal(result.arrowCss.right, "auto");
});

module("Inline Dialog Hiding", {
    setup: function() {
        AJS.$.fx.off = true;
        this.$link = AJS.$("<div class='test-link'></div>");
        this.$popupContainer = AJS.$("<div class='popup-container'></div>");
        var $styles = AJS.$("<style type='text/css'>.aui-inline-dialog { display: none; }</style>");
        AJS.$("#qunit-fixture").append($styles).append(this.$link).append(this.$popupContainer);
    },
    teardown: function() {
        AJS.$.fx.off = false;
    },
    renderDialog: function(options) {
        var defaultOptions = {
            container: '.popup-container'
        };
        var renderPopupContent = function (content, trigger, showPopup) {
            showPopup();
        };
        this.dialog = AJS.InlineDialog(this.$link, 1, renderPopupContent, AJS.$.extend({}, defaultOptions, options));
        this.dialog.show();
        this.clock.tick();
    },
    pressEsc: function() {
        var e = AJS.$.Event("keydown");
        e.keyCode = 27;
        AJS.$(document).trigger(e);
        this.clock.tick();
    }
});

test("Dialog hides when escape is pressed", function() {
    this.renderDialog();
    equal(this.dialog.css('display'), 'block', 'the dialog should render as visible');

    this.pressEsc();
    equal(this.dialog.css('display'), 'none', 'the dialog should be hidden once esc is pressed');
});

test("Dialog unregisters to keydown event when hidden", function() {
    this.spy(AJS.$.prototype, 'off');
    this.renderDialog();
    this.pressEsc();

    sinon.assert.calledWith(AJS.$.prototype.off, 'keydown', sinon.match.func);
});

test("Dialog does not hide when esc is pressed and persistent is enabled", function() {
    this.renderDialog({persistent: true});
    this.pressEsc();

    equal(this.dialog.css('display'), 'block');
});