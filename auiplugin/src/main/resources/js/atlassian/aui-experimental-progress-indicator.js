//API
AJS.progressBars = {
    update: function(element, value){
        var $progressBarContainer = AJS.$(element).first();
        var $progressBar = $progressBarContainer.children(".aui-progress-indicator-value");
        var currentProgress = $progressBar.attr("data-value") || 0;

        var afterTransitionEvent = "aui-progress-indicator-after-update";
        var beforeTransitionEvent = "aui-progress-indicator-before-update";
        var transitionEnd = "transitionend webkitTransitionEnd";

        var isIndeterminate = !$progressBarContainer.attr("data-value");

        //if the progress bar is indeterminate switch it.
        if(isIndeterminate){
            $progressBar.detach().css("width", 0).appendTo($progressBarContainer);
        }

        if(typeof value === "number" && value<= 1 && value >= 0){
            //trigger before animation event
            $progressBarContainer.trigger(beforeTransitionEvent, [currentProgress, value]);

            //trigger after animation event

            //detect whether transitions are supported
            var documentBody = document.body || document.documentElement;   
            var style = documentBody.style;
            var transition = 'transition';

            function updateProgress(value) {
                //update the progress bar, need to set timeout so that batching of dom updates doesn't happen
                window.setTimeout(function(){
                    $progressBar.css("width", value * 100 + "%")
                    $progressBarContainer.attr("data-value", value);
                }, 0);   
            }
            
            //trigger the event after transition end if supported, otherwise just trigger it
            if(typeof style.transition === 'string' || typeof style.WebkitTransition === "string"){
                $progressBar.one(transitionEnd, function(){
                    $progressBarContainer.trigger(afterTransitionEvent, [currentProgress, value]);
                });
                updateProgress(value);
            } else {
                updateProgress(value);
                $progressBarContainer.trigger(afterTransitionEvent, [currentProgress, value]);
            }


        }
        return $progressBarContainer;
    },
    setIndeterminate: function(element){
        var $progressBarContainer = AJS.$(element).first();
        var $progressBar = $progressBarContainer.children(".aui-progress-indicator-value");

        $progressBarContainer.removeAttr("data-value");
        $progressBar.css("width", "100%");
    }
};

