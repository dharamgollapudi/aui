package it.com.atlassian.aui.javascript.integrationTests;

import com.atlassian.pageobjects.aui.component.dropdown2.AuiDropdown2;
import com.atlassian.pageobjects.aui.component.dropdown2.AuiDropdown2Item;
import it.com.atlassian.aui.javascript.AbstractAuiIntegrationTest;
import it.com.atlassian.aui.javascript.pages.Dropdown2TestPage;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class AUIDropdown2SubmenuTest extends AbstractAuiIntegrationTest
{
    private Dropdown2TestPage dropdown2TestPage;

    @Before
    public void setUp() {
        dropdown2TestPage = product.visit(Dropdown2TestPage.class);
        dropdown2TestPage.getCustomDropdownContainerElement().click();
    }


    private AuiDropdown2 openDropdown()
    {
        AuiDropdown2 dropdown = dropdown2TestPage.getSubmenuEditDropdown();
        dropdown.open();
        return dropdown;
    }

    private AuiDropdown2 openFindSubmenu(AuiDropdown2 dropdown)
    {
        AuiDropdown2Item itemWithSubmenu = dropdown.getDropdownItems().get(2);
        itemWithSubmenu.hoverOver();
        return itemWithSubmenu.getSubmenu();
    }

    @Test
    public void testSubmenuOpensOnHover()
    {
        AuiDropdown2 dropdown = openDropdown();
        AuiDropdown2 submenu = openFindSubmenu(dropdown);

        assertTrue("Submenu should be open", submenu.isOpen());
    }

    @Test
    public void testSubmenuCloses()
    {
        AuiDropdown2 dropdown = openDropdown();
        AuiDropdown2 submenu = openFindSubmenu(dropdown);
        dropdown.close();

        assertTrue("Submenu should be closed too", !submenu.isOpen());
    }

    @Test
    public void testSubmenuClosesWhenSelectingOtherItem()
    {
        AuiDropdown2 dropdown = openDropdown();
        AuiDropdown2 submenu = openFindSubmenu(dropdown);

        dropdown.getDropdownItems().get(3).hoverOver();

        assertTrue("Submenu should be closed", !submenu.isOpen());
    }

    @Test
    @Ignore("Flaky test not finding elements that work in manual check https://ecosystem.atlassian.net/browse/AUI-1343")
    public void testSubmenuCanContainAnotherSubmenu()
    {
        AuiDropdown2 dropdown = openDropdown();
        AuiDropdown2 submenu = openFindSubmenu(dropdown);
        AuiDropdown2Item findMoreSubmenuItem = submenu.getDropdownItems().get(2);
        findMoreSubmenuItem.hoverOver();
        AuiDropdown2 findMoreSubmenu = findMoreSubmenuItem.getSubmenu();

        assertTrue("Main dropdown should be open", dropdown.isOpen());
        assertTrue("First submenu should be open", submenu.isOpen());
        assertTrue("Second submenu should be open", findMoreSubmenu.isOpen());
    }


    @Test
    public void testEnterOpensSubmenu()
    {
        AuiDropdown2 dropdown = openDropdown();
        dropdown.selectNextItem();
        dropdown.pressReturn();
        AuiDropdown2Item itemWithSubmenu = dropdown.getActiveItem();

        assertTrue("Main dropdown should be open", dropdown.isOpen());
        assertTrue("submenu should be open", itemWithSubmenu.getSubmenu().isOpen());
    }

    @Test
    public void testRightArrowOpensSubmenu()
    {
        AuiDropdown2 dropdown = openDropdown();
        dropdown.selectNextItem();
        dropdown.pressRight();
        AuiDropdown2Item itemWithSubmenu = dropdown.getActiveItem();

        assertTrue("Main dropdown should be open", dropdown.isOpen());
        assertTrue("submenu should be open", itemWithSubmenu.getSubmenu().isOpen());
    }

    @Test
    public void testLeftArrowClosesSubmenu()
    {
        AuiDropdown2 dropdown = openDropdown();
        dropdown.selectNextItem();
        dropdown.pressRight();
        AuiDropdown2Item itemWithSubmenu = dropdown.getActiveItem();
        itemWithSubmenu.getSubmenu().pressLeft();

        assertTrue("Main dropdown should be open", dropdown.isOpen());
        assertTrue("submenu should be closed", !itemWithSubmenu.getSubmenu().isOpen());
    }

    @Test
    public void testEscapeOnlyClosesSubmenu()
    {
        AuiDropdown2 dropdown = openDropdown();
        dropdown.selectNextItem();
        dropdown.pressReturn();

        AuiDropdown2 submenu = dropdown.getActiveItem().getSubmenu();
        submenu.closeWithEscape();

        assertTrue("Main dropdown should be open", dropdown.isOpen());
        assertTrue("submenu should be closed", !submenu.isOpen());
    }

}
