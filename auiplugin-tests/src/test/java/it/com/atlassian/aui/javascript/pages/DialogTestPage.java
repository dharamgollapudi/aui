package it.com.atlassian.aui.javascript.pages;

import com.atlassian.pageobjects.Page;
import com.atlassian.pageobjects.elements.PageElementFinder;

import javax.inject.Inject;

/**
 * @since 3.7
 */
public class DialogTestPage extends TestPage
{
    public String getUrl()
    {
        return "/plugins/servlet/ajstest/test-pages/dialog/dialog-test.html";
    }
}
