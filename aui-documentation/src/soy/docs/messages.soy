
{namespace flatpack.docs}

/**
 * Test page
 **/
{template .messages}
    {call flatpack.docTemplate}
        {param id: 'messages' /}
        {param componentName: 'Messages' /}
        {param sandboxURL: '../sandbox/index.html?component=messages'/}
        {param adgURL: 'https://developer.atlassian.com/design/latest/messages.html'/}
        {param apiStatus: 'general' /}
        {param inCore: true /}
        {param webResource: 'com.atlassian.auiplugin:aui-messages' /}
        {param versionAvailable: '3.0' /}
        {param summary}

          <p>Messages are the primary method for providing system feedback in the product user interface. Messages include notifications of various kinds: alerts, confirmations, notices, warnings, info and errors.</p>

        {/param}
        {param examples}
            {call flatpack.example}
                {param extraClasses}messages-example{/param}
                {param html}{call demo.messages.html /}{/param}
                {param javascript}{call demo.messages.javascript /}{/param}
            {/call}
        {/param}
        {param code}

            <p>There are several message types with different colours and icons. There are two ways to implement messages: using HTML (or Soy to generate the HTML) or using Javascript.</p>

            <h4>HTML</h4>

            {call flatpack.docExample}
            {param html}{literal}<div class="aui-message error">
    <p class="title">
        <span class="aui-icon icon-error"></span>
        <strong>Error!</strong>
    </p>
    <p>And this is just content in a Default message.</p>
</div>{/literal}{/param}
            {/call}

            <h4>Javascript</h4>
            <p>Note you should use the JS API if adding Messages with JavaScript, not the compiled-Javascript-Soy API. Although the two seems similar, the JS API is the supported option.</p>
            <p>Default context:</p>

            {call flatpack.docExample}
            {param js}{literal}AJS.messages.generic({
   title:"This is a title in a Default message.",
   body: "<p> And this is just content in a Default message.</p>"
});{/literal}{/param}
            {/call}

            <p>Note if you do not set the context, the element:</p>

            {call flatpack.docExample}
            {param html}{literal}<div id="aui-message-bar"></div>{/literal}{/param}
            {/call}
            
            <p>...<strong>must</strong> exist in your document, otherwise the message will not appear as it has no target location.</p>

            <p>It is more common to set the context:</p>

            {call flatpack.docExample}
            {param js}{literal}AJS.messages.generic("#context", {
   title:"This is a title in a Default message.",
   body: "<p> And this is just content in a Default message.</p>"
});{/literal}{/param}
            {/call}

            <h4>Soy</h4>

            {call flatpack.docExample}
            {param soy}
            {literal}{call aui.message.warning}
    {param title: 'An error occurred - user intervention required!' /}
    {param closeable: 'true' /}
    {param id: 'messageIDattribute' /}
    {param content}
        <p>Some details about the error so the user knows what to do.</p>
    {/param}
{/call}{/literal}
            {/param}
            {/call}

        {/param}
        {param options}

            <p>When adding an HTML message, you must ensure root element (.aui-message) has the desired message class; and the corresponding icon class must also be set. Calling from Soy or JavaScript wraps this into one call, for convenience.</p>

            <table class="aui">
              <thead>
                <tr>
                  <th>Message type</th>
                  <th>Message class</th>
                  <th>Icon class</th>
                  <th>Soy call</th>
                  <th>Javascript function</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                    <td>Generic</td>
                    <td><code>generic</code></td>
                    <td><code>icon-generic</code></td>
                    <td><code>call aui.message.generic</code></td>
                    <td><code>AJS.messages.generic()</code></td>
                </tr>
                <tr>
                    <td>Error</td>
                    <td><code>error</code></td>
                    <td><code>icon-error</code></td>
                    <td><code>call aui.message.error</code></td>
                    <td><code>AJS.messages.error()</code></td>
                </tr>
                <tr>
                    <td>Warning</td>
                    <td><code>warning</code></td>
                    <td><code>icon-warning</code></td>
                    <td><code>call aui.message.warning</code></td>
                    <td><code>AJS.messages.warning()</code></td>
                </tr>
                <tr>
                    <td>Success</td>
                    <td><code>success</code></td>
                    <td><code>icon-success</code></td>
                    <td><code>call aui.message.success</code></td>
                    <td><code>AJS.messages.success()</code></td>
                </tr>
                <tr>
                    <td>Info</td>
                    <td><code>info</code></td>
                    <td><code>icon-info</code></td>
                    <td><code>call aui.message.info</code></td>
                    <td><code>AJS.messages.info()</code></td>
                </tr>
                <tr>
                    <td>Hint</td>
                    <td><code>hint</code></td>
                    <td><code>icon-hint</code></td>
                    <td><code>call aui.message.hint</code></td>
                    <td><code>AJS.messages.hint()</code></td>
                </tr>
              </tbody>
            </table>

            <h4>HTML options</h4>
            <p>These options are set by adding classes to the root <code>aui-message</code> div.</p>
            <table class="aui">
              <thead>
                <tr>
                  <th>Class</th>
                  <th>Effect</th>
                  <th>Example</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                    <td><code>closeable</code></td>
                    <td>Adds a Close icon to the message which closes and removes the message when clicked.</td>
                    <td>
                        {call flatpack.docExample}
                        {param html}{literal}<div class="aui-message closeable">...</div>{/literal}{/param}
                        {/call}
                    </td>
                </tr>
                <tr>
                    <td><code>fadeout</code></td>
                    <td>Since 5.1. Makes the message fade away after five seconds. The fadeout will be cancelled if the user interacts with it (hover or focus). Note the fadeout option is best used via JavaScript and should not be used on critical errors and other information the user must be aware of.</td>
                    <td>
                        {call flatpack.docExample}
                        {param html}{literal}<div class="aui-message fadeout">...</div>{/literal}{/param}
                        {/call}
                    </td>
                </tr>
              </tbody>
            </table>

            <h4>Soy options</h4>
            <p>These options are set by adding params to the Soy call.</p>
            <table class="aui">
              <thead>
                <tr>
                  <th>Param</th>
                  <th>Effect</th>
                  <th>Default</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                    <td>content</td>
                    <td>Required. Content to display within the message.</td>
                    <td>n/a</td>
                </tr>
                <tr>
                    <td>titleContent</td>
                    <td>Title text of the message.</td>
                    <td>n/a</td>
                </tr>
                <tr>
                    <td>id</td>
                    <td>ID attribute</td>
                    <td>n/a</td>
                </tr>
                <tr>
                    <td>isCloseable</td>
                    <td>Boolean. Set to true, makes the Message closeable.</td>
                    <td>false</td>
                </tr>
              </tbody>
            </table>

            <h4>JavaScript options</h4>
            <p>These options are set in the options object when creating a Message with JavaScript:</p>
            <table class="aui">
              <thead>
                <tr>
                  <th>Option</th>
                  <th>Description</th>
                  <th>Possible values</th>
                  <th>Default</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>(context argument)</td>
                  <td>You can override the default context by passing it into the first argument of the messages function. This is the only option set as an argument.</td>
                  <td>A string in the form of an ID selector</td>
                  <td>#aui-message-bar</td>
                </tr>
                <tr>
                  <td><code>body</code></td>
                  <td>The main content of the message.</td>
                  <td>HTML</td>
                  <td>none</td>
                </tr>
                <tr>
                  <td><code>closeable</code></td>
                  <td>Adds a control allowing the user to close the message, removing it from the page.</td>
                  <td>boolean</td>
                  <td>true</td>
                </tr>
                <tr>
                  <td><code>id</code></td>
                  <td>Gives your message an ID attribute, useful for selecting the message later.</td>
                  <td>ID string (no hash)</td>
                  <td>none</td>
                </tr>
                <tr>
                  <td><code>insert</code></td>
                  <td>Sets the insert point to the start (prepent) or end (append) of the context element. (Option added in AUI 4.2)</td>
                  <td>prepend, append</td>
                  <td>append</td>
                </tr>
                <tr>
                  <td><code>shadowed</code></td>
                  <td>Toggles the dropshadow on the message box. Usually not changed from default.</td>
                  <td>boolean</td>
                  <td>true</td>
                </tr>
                <tr>
                  <td><code>title</code></td>
                  <td>Sets the title text of the message.</td>
                  <td>Plain text</td>
                  <td>none</td>
                </tr>
                <tr>
                  <td><code>fadeout</code></td>
                  <td>(since 5.1) Toggles the fade away on the message</td>
                  <td>boolean</td>
                  <td>false</td>
                </tr>
                <tr>
                  <td><code>delay</code></td>
                  <td>(since 5.1) Time to wait (in ms) before starting fadeout animation (ignored if fadeout==false)</td>
                  <td>number</td>
                  <td>5000</td>
                </tr>
                <tr>
                  <td><code>duration</code></td>
                  <td>(since 5.1) Fadeout animation duration in milliseconds (ignored if fadeout==false)</td>
                  <td>number</td>
                  <td>500</td>
                </tr>
              </tbody>
            </table>

            <h4>AJS.messages events</h4>
            <table class="aui">
              <thead>
                <tr>
                  <th>Event</th>
                  <th>Description</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>messageClose</td>
                  <td><span class="aui-lozenge aui-lozenge-current">Deprecated</span> <code>messageClose</code>. When a message is closed, messageClose is fired before the message is removed from the DOM, including a reference to the DOM element being removed.</td>
                </tr>
                <tr>
                  <td>aui-message-close</td>
                  <td><code>messageClose</code>. When a message is closed, aui-message-close is fired AFTER the element is removed, a reference to the message being removed is included in the event data.</td>
                </tr>
              </tbody>
            </table>
            {call flatpack.docExample}
            {param js}{literal}AJS.$(document).on("aui-message-close", function (e,a) {
    AJS.log("message id: " + a.attr("id"));
});{/literal}{/param}
            {/call}

        {/param}
    {/call}
{/template}